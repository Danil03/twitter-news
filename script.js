class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
        this.element = this.createCardElement();
        this.setupDeleteButton();
    }

    createCardElement() {
        const card = document.createElement('div');
        card.classList.add('card');

        const header = document.createElement('h2');
        header.textContent = this.post.title;

        const body = document.createElement('p');
        body.textContent = this.post.body;

        const authorInfo = document.createElement('p');
        authorInfo.textContent = `Автор: ${this.user.name} ${this.user.surname}, електронна адреса: ${this.user.email}`;

        card.appendChild(header);
        card.appendChild(body);
        card.appendChild(authorInfo);

        return card;
    }

    setupDeleteButton() {
        const deleteButton = document.createElement('button');
        deleteButton.textContent = 'Delete';
        deleteButton.addEventListener('click', () => {
            this.deletePost(this.post.id); 
            this.element.remove();
        });
        this.element.appendChild(deleteButton);
    }
    
    deletePost() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: 'DELETE'
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Помилка при видаленні публікації: ' + response.statusText);
                }
                return response.json();
            })
            .then(() => {
                this.element.remove();
                console.log('Публікація успішно видалена');
            })
            .catch(error => {
                console.error('Помилка при видаленні публікації:', error);
            });
    }
}

document.addEventListener('DOMContentLoaded', function () {
    fetch('https://ajax.test-danit.com/api/json/users')
        .then(response => {
            if (!response.ok) {
                throw new Error('Помилка при отриманні даних про користувачів: ' + response.statusText);
            }
            return response.json();
        })
        .then(users => {
            fetch('https://ajax.test-danit.com/api/json/posts')
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Помилка при отриманні даних про публікації: ' + response.statusText);
                    }
                    return response.json();
                })
                .then(posts => {
                    displayPosts(posts, users);
                })
                .catch(error => {
                    console.error(error);
                });
        })
        .catch(error => {
            console.error(error);
        });
});

function displayPosts(posts, users) {
    const postsContainer = document.getElementById('posts-container');
    if (!postsContainer) {
        console.error('Елемент posts-container не знайдено в документі.');
        return;
    }

    posts.forEach(post => {
        const user = users.find(user => user.id === post.userId);
        const card = new Card(post, user);
        postsContainer.appendChild(card.element);
    });
}





//   const xhr = new XMLHttpRequest();

// xhr.open('GET', 'https://ajax.test-danit.com/api/swapi/films', true);

// xhr.onload = function() {

//   if (xhr.status >= 200 && xhr.status < 300) {

//     const films = JSON.parse(xhr.responseText);
    
//     displayFilms(films);
    
//     films.forEach(function(film) {
//       getCharactersForFilm(film);
//     });
//   } else {
//     console.error('Помилка при завантаженні даних:', xhr.statusText);
//   }
// };

// xhr.send();

// function displayFilms(films) {
//   const filmListContainer = document.getElementById('film-list');
//   films.forEach(function(film) {
//     const filmElement = document.createElement('div');
//     filmElement.classList.add('film');
//     filmElement.innerHTML = '<h3>Episode ' + film.episodeId + ': ' + film.title + '</h3>' +
//                             '<p>' + film.openingCrawl + '</p>';
//     filmElement.setAttribute('id', 'film-' + film.episodeId); 
//     filmListContainer.appendChild(filmElement);
//   });
// }


// function getCharactersForFilm(film) {
//   film.characters.forEach(function(characterURL) {
//     const characterXhr = new XMLHttpRequest();
//     characterXhr.open('GET', characterURL, true);
//     characterXhr.onload = function() {
//       if (characterXhr.status >= 200 && characterXhr.status < 300) {
//         const characterData = JSON.parse(characterXhr.responseText);
//         const filmElement = document.getElementById('film-' + film.episodeId);
//         const characterList = filmElement.querySelector('.character-list') || document.createElement('ul');
//         const characterItem = document.createElement('li');
//         characterItem.textContent = characterData.name;
//         characterList.appendChild(characterItem);
//         filmElement.appendChild(characterList);
//       } else {
//         console.error('Помилка при отриманні персонажів:', characterXhr.statusText);
//       }
//     };
//     characterXhr.onerror = function() {
//       console.error('Помилка при виконанні запиту персонажів.');
//     };
//     characterXhr.send();
//   });
// }